# Contributing

## Getting Started

Make sure you've got `python` and `pip` installed.

```sh
python --version
pip --version
```

This project uses the [Pipenv](https://pipenv.kennethreitz.org/) dependency manager:

```sh
pip install --user pipenv
pipenv install
```

## Build & Deployment

Install the development dependencies:

```sh
pipenv install --dev
```

Update the `requirements.txt` file:

```sh
pipenv requirements > requirements.txt
```

Update the `version`:

- in the `version` field in [pyproject.toml](./pyproject.toml)
- in the `docopts` version string in [cli.py](./imagemv/cli.py)

```sh
version='X.Y.Z'
git add pyproject.toml imagemv/cli.py
git commit -m "Update version -> v${version}"
```

Build and deploy the package:

```sh
rm -r dist/* && rm -r imagemv.egg-info && python -m build
python -m twine upload [--repository testpypi] dist/*

```

Tag the release:

```sh
git tag -a "v${version}" -m "Release ${version}" && git push origin "v${version}"
```
