#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from imagemv.cli import main


main()

# debug:
"""
import os

source = os.path.join("")
destination = os.path.join("")

arguments = {
    "SOURCE": source,
    "DEST": destination,
    "--keep-all": True,
    "--keep": 0,
    "--verbose": True,
    "--dry-run": True,
}

main(arguments)
"""
